#include "extract.h"

int main(int argc, char ** argv) {
	if (argc < 2) {
		std::cout << argv[0] << " <device / file>" << std::endl;
		return 1;
	}
	int const blocksize = 50 * 1000 * 1000;
	std::string data;
    data.resize(blocksize);
	std::ifstream file(argv[1], std::ios::in|std::ios::binary);
	if (file.is_open()) {
		file.seekg(0, std::ios::beg);
		while (file.tellg() != -1) {
			file.read(&data[0], blocksize);
			//std::cout << data<< std::endl;
			std::cout << file.tellg()/1000000 << std::endl;
		}
		file.close();
	}
	else {
		std::cout << "Failed to open file" << std::endl;
	}
}
